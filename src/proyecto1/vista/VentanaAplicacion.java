package proyecto1.vista;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.Point;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;
import javax.swing.filechooser.FileFilter;
import proyecto1.control.ControlProyecto;
import proyecto1.utils.ConversorGrados;
import xmlproyecto1.Latitud;
import xmlproyecto1.Longitud;
import proyecto1.modelo.entidades.ListaTerremotos;

/**
 *
 * @author Matthew Miranda
 */
public class VentanaAplicacion extends JFrame implements Observer {

    private Latitud latMouse;
    private Longitud longMouse;

    private final ControlProyecto gestorPrincipal;
    private final boolean cargaXML;
    private JScrollPane controlDesplazamientoMapa;
    private JMenuBar menuPrincipal;
    private JMenu menuArchivo;
    private JMenuItem itemSalir;
    private JMenuItem itemVentanaFiltrar;
    private JMenuItem itemVentanaTabla;
    private JMenuItem itemSeleccionarArchivo;
    private JMenuItem itemCreditos;
    private JMenuItem itemAtajos;
    private BarraEstado barraEstado;
    private VisorSismos visor;
    private JMenu menuVentana;
    private JMenu menuInformacion;

    public VentanaAplicacion(String titulo, ControlProyecto nuevoGestor, boolean cargado) {
        super(titulo);
        this.gestorPrincipal = nuevoGestor;
        cargaXML = cargado;
        configurarVentana();
    }

    private void configurarVentana() {
        ajustarComponentes(getContentPane());
        setResizable(true);
        setSize(840, 750);
        setLocationRelativeTo(null);
        setMinimumSize(new Dimension(640, 480));
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                cerrarAplicacion();
            }
        });
        addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.isControlDown() && e.getKeyCode() == KeyEvent.VK_O) {
                    seleccionarArchivoDatos();
                }
                if (e.isControlDown() && e.getKeyCode() == KeyEvent.VK_F) {
                    abrirVentanaFiltrar();
                }
                if (e.isControlDown() && e.getKeyCode() == KeyEvent.VK_T) {
                    abrirVentanaTabla();
                }
                if (e.isControlDown() && e.getKeyCode() == KeyEvent.VK_Q) {
                    cerrarAplicacion();
                }
                if (e.getKeyCode() == KeyEvent.VK_H) {
                    visor.ocultar_mostrar_terremotos();
                }
            }
        });                
    }

    public void ajustarComponentes(Container c) {
        c.setLayout(new BorderLayout());

        ajustarMenu();

        JPanel principal = new JPanel();
        principal.setLayout(new BorderLayout());

        controlDesplazamientoMapa = new JScrollPane(
                visor = new VisorSismos(gestorPrincipal.obtenerDatosXML()));
        principal.setLayout(new BorderLayout());
        principal.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        principal.add(BorderLayout.CENTER, controlDesplazamientoMapa);
        c.add(BorderLayout.CENTER, principal);
        visor.registrarObservador(this);
        c.add(BorderLayout.PAGE_END, barraEstado = new BarraEstado());
        barraEstado.mostrarMensaje(cargaXML ? "map.xml cargado correctamente" : "error al cargar map.xml");

    }

    public void init() {
        gestorPrincipal.registrar(this);
        setVisible(true);
    }

    private void ajustarMenu() {
        menuPrincipal = new JMenuBar();
        menuPrincipal.add(menuArchivo = new JMenu("Archivo"));
        menuPrincipal.add(menuVentana = new JMenu("Edicion"));
        menuPrincipal.add(menuInformacion = new JMenu("Ayuda"));
        menuArchivo.add(itemSeleccionarArchivo = new JMenuItem("Seleccionar Fichero de Datos"));
        menuArchivo.add(itemSalir = new JMenuItem("Salir"));
        menuVentana.add(itemVentanaFiltrar = new JMenuItem("Filtrar Eventos"));
        menuVentana.add(itemVentanaTabla = new JMenuItem("Tabla de Eventos"));
        menuInformacion.add(itemAtajos = new JMenuItem("Atajos de Teclado"));
        menuInformacion.add(itemCreditos = new JMenuItem("Creditos"));
        setJMenuBar(menuPrincipal);

        itemSeleccionarArchivo.addActionListener((e) -> {
            seleccionarArchivoDatos();
        });

        itemVentanaFiltrar.addActionListener((e) -> {
            abrirVentanaFiltrar();
        });

        itemVentanaTabla.addActionListener((e) -> {
            abrirVentanaTabla();
        });

        itemAtajos.addActionListener((e) -> {
            mostrarAtajos();
        });

        itemCreditos.addActionListener((e) -> {
            creditos();
        });

        itemSalir.addActionListener((e) -> {
            cerrarAplicacion();
        });

    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg.getClass().equals(Point.class)) {
            Point punto = (Point) arg;
            latMouse = ConversorGrados.convertirLatitud_Sexagecimal(
                    ConversorGrados.convertirPixeles_GeograficasLatitud(punto.y));
            longMouse = ConversorGrados.convertirLongitud_Sexagecimal(
                    ConversorGrados.convertirPixeles_GeograficasLongitud(punto.x));
            barraEstado.mostrarMensaje("(" + latMouse.toString() + " , " + longMouse.toString() + ")");
        } else if (arg.getClass().equals(ListaTerremotos.class)) {
            ListaTerremotos lt = (ListaTerremotos) arg;
            visor.limpiarLista();
            for (int i = 0; i < lt.cantidadTerremotos(); i++) {
                visor.anadirTerremoto(lt.obtenerTerremoto(i));
            }
        }
    }

    public void cerrarAplicacion() {
        System.out.println("Cerrando la aplicación..");
        gestorPrincipal.suprimir(this);
        gestorPrincipal.cerrarAplicacion();
    }

    @Override
    public String toString() {
        return String.format("VentanaAplicacion('%s')", getTitle());
    }

    public void seleccionarArchivoDatos() {
        JFileChooser jfc = new JFileChooser();
        jfc.setFileFilter(new FileFilter() {
            @Override
            public String getDescription() {
                return "Archivos de Texto (*.txt)";
            }

            @Override
            public boolean accept(File f) {
                if (f.isDirectory()) {
                    return true;
                } else {
                    return f.getName().toLowerCase().endsWith(".txt");
                }
            }

        });
        if (jfc.showOpenDialog(jfc) == JFileChooser.APPROVE_OPTION) {
            cargarArchivo(jfc.getSelectedFile());
        }
    }

    public void cargarArchivo(File archivo) {
        gestorPrincipal.cargarTerremotos(archivo);
    }

    public void abrirVentanaFiltrar() {
        System.out.println("Inicializando Ventana de Filtracion de Eventos.");
        new VentanaFiltracion("Ventana de Filtracion y Busqueda de Eventos", gestorPrincipal).init();
    }

    public void abrirVentanaTabla() {
        System.out.println("Inicializando Ventana Visualizacion de la tabla.");
        new VentanaTabla("Ventana de Tabla de Informacion", gestorPrincipal).init();
    }

    private void creditos() {
        JOptionPane.showMessageDialog(null, "Proyecto desarrollado por:\n\n"
                + "\tMatthew Miranda Araya\n"
                + "\tKenneth Sibaja Castro");
    }

    private void mostrarAtajos() {
        JOptionPane.showMessageDialog(null, "Atajos del teclado:\n"
                + "\n H - Ocultar/Mostrar terremotos en el mapa"
                + "\n Ctrl + O - Abrir archivo de datos"
                + "\n Ctrl + F - Abrir ventana de filtracion de datos"
                + "\n Ctrl + T - Abrir ventana de tabla de datos"
                + "\n Ctrl + Q - Salir de la Aplicacion");
    }
}
