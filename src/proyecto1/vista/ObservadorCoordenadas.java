/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto1.vista;

import java.util.Observable;

/**
 *
 * @author Kenneth Sibaja
 */
public class ObservadorCoordenadas extends Observable{
        
    public ObservadorCoordenadas(){
        super();
    }
    
    public void notificar(Object obj){
        setChanged();
        notifyObservers(obj);
        clearChanged();
    }
   
}
