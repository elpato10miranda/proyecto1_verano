/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto1.vista;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.Observable;
import java.util.Observer;
import javax.swing.BorderFactory;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;
import javax.swing.filechooser.FileFilter;
import proyecto1.control.ControlProyecto;
import proyecto1.modelo.ModeloColumnas;
import proyecto1.modelo.ModeloTablaTerremotos;
import proyecto1.modelo.entidades.ListaTerremotos;
import proyecto1.vista.tabla.TablaTerremotos;

/**
 *
 * @author Kenneth Sibaja
 */
public class VentanaTabla extends JFrame implements Observer {

    private final ControlProyecto gestorPrincipal;
    private JMenuBar menuPrincipal;
    private JMenu menuArchivo;
    private JMenuItem itemSeleccionarArchivo;
    private JMenuItem itemSalir;
    private JScrollPane controlDesplazamientoTabla;
    private TablaTerremotos tablaTerremotos;

    public VentanaTabla(String titulo, ControlProyecto nuevoGestor) throws HeadlessException {
        super(titulo);
        this.gestorPrincipal = nuevoGestor;
        configurarVentana();
    }

    private void configurarVentana() {
        ajustarComponentes(getContentPane());
        setResizable(true);
        setSize(800, 600);
        setMinimumSize(new Dimension(800, 600));
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                cerrarVentana();
            }
        });
    }

    public void ajustarComponentes(Container c) {
        c.setLayout(new BorderLayout());

        ajustarMenu();

        JPanel principal = new JPanel();
        principal.setLayout(new BorderLayout());
        principal.setBorder(BorderFactory.createEmptyBorder(8, 8, 8, 8));

        controlDesplazamientoTabla = new JScrollPane(
                tablaTerremotos = new TablaTerremotos(
                        new ModeloTablaTerremotos((ListaTerremotos) gestorPrincipal.obtenerListaTerremotos()),
                        new ModeloColumnas())
        );

        gestorPrincipal.registrar((Observer) tablaTerremotos.getModel());

        principal.add(BorderLayout.CENTER, controlDesplazamientoTabla);

        c.add(BorderLayout.CENTER, principal);

    }

    public void init() {
        gestorPrincipal.registrar(this);
        setVisible(true);
    }

    private void ajustarMenu() {
        menuPrincipal = new JMenuBar();
        menuPrincipal.add(menuArchivo = new JMenu("Archivo"));
        menuArchivo.add(itemSeleccionarArchivo = new JMenuItem("Seleccionar Fichero de Datos"));
        menuArchivo.add(itemSalir = new JMenuItem("Salir"));
        setJMenuBar(menuPrincipal);

        itemSeleccionarArchivo.addActionListener((e) -> {
            seleccionarArchivoDatos();
        });

        itemSalir.addActionListener((e) -> {
            cerrarPrograma();
        });
    }

    @Override
    public void update(Observable o, Object arg) {

    }

    public void seleccionarArchivoDatos() {
        JFileChooser jfc = new JFileChooser();
        jfc.setFileFilter(new FileFilter() {
            @Override
            public String getDescription() {
                return "Archivos de Texto (*.txt)";
            }

            @Override
            public boolean accept(File f) {
                if (f.isDirectory()) {
                    return true;
                } else {
                    return f.getName().toLowerCase().endsWith(".txt");
                }
            }

        });
        if (jfc.showOpenDialog(jfc) == JFileChooser.APPROVE_OPTION) {
            cargarArchivo(jfc.getSelectedFile());
        }
    }

    public void cargarArchivo(File archivo) {
        gestorPrincipal.cargarTerremotos(archivo);
    }

    public void cerrarVentana() {
        gestorPrincipal.suprimir(this);
    }

    public void cerrarPrograma() {
        System.out.println("Cerrando la aplicación..");
        gestorPrincipal.suprimir(this);
        gestorPrincipal.cerrarAplicacion();
    }
}
