package proyecto1.vista;

import java.awt.Color;
import java.awt.Point;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Base64;
import proyecto1.configuracion.Configuracion;
import proyecto1.utils.ConversorGrados;
import proyecto1.modelo.entidades.Terremoto;

public class AreaTerremoto {

    private Shape areaTerremoto;
    private Color color;
    private final Point epicentro;
    private final Terremoto terremoto;
    private double diametroArea;
    private double x, y;

    public AreaTerremoto(Terremoto terremoto) {
        this.terremoto = terremoto;
        epicentro = new Point(
                ConversorGrados.convertirLongitudSexagecimal_pixel(terremoto.getLongitud()),
                ConversorGrados.convertirLatitudSexagecimal_pixel(terremoto.getLatitud()));
        configurarTerremoto();
    }

    private void configurarTerremoto() {
        
        //  Obtenemos el color desde el archivo de propiedades, dependiendo de la magnitud del terremoto
        String magnitud = String.valueOf((int)terremoto.getMagnitud());
        int auxColor = Integer.parseInt((String)Configuracion.obtenerInstancia().get("c"+magnitud));
        this.color = new Color(-auxColor, true);

        // Diametro base del circulo es de 20 pixeles + el tamaño obtenido por la magnitud del terremoto
        diametroArea = 20 + Integer.parseInt((String)Configuracion.obtenerInstancia().get("t"+magnitud));

        double radio = diametroArea;
        radio /= 2;
        x = epicentro.x;
        y = epicentro.y;
        x -= radio;
        y -= radio;

    }

    public Shape crearArea(int xMapa, int yMapa) {
        areaTerremoto = new Ellipse2D.Double(x + xMapa, y + yMapa, diametroArea, diametroArea);
        return areaTerremoto;
    }

    public Color getColor() {
        return color;
    }

    public Shape getAreaTerremoto() {
        return areaTerremoto;
    }

    public void setAreaTerremoto(Shape areaTerremoto) {
        this.areaTerremoto = areaTerremoto;
    }

    public Terremoto getTerremoto() {
        return terremoto;
    }

    public Point getEpicentro() {
        return epicentro;
    }

    public double getDiametroArea() {
        return diametroArea;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

}
