package proyecto1.vista;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.Insets;
import java.awt.Label;
import java.util.List;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.filechooser.FileFilter;
import proyecto1.control.ControlProyecto;

/**
 *
 * @author Kenneth Sibaja
 */
public class VentanaFiltracion extends JFrame implements Observer {

    private final ControlProyecto gestorPrincipal;
    private JMenuBar menuPrincipal;
    private JMenu menuArchivo;
    private JMenuItem itemSeleccionarArchivo;
    private JMenuItem itemSalir;
    private final JTextField fechaIni = new JTextField("");
    private final JTextField fechaFin = new JTextField("");
    private final JTextField magnitudMin = new JTextField("");
    private final JTextField magnitudMax = new JTextField("");
    private final JButton btnFiltrar = new JButton("Filtrar");
    private final JRadioButton rbCualquierMagnitud = new JRadioButton("Cualquier Magnitud");
    private final List<JTextField> listTXT;

    public VentanaFiltracion(String titulo, ControlProyecto nuevoGestor) throws HeadlessException {
        super(titulo);
        this.gestorPrincipal = nuevoGestor;
        listTXT = new ArrayList<>();
        listTXT.add(fechaIni);
        listTXT.add(fechaFin);
        listTXT.add(magnitudMax);
        listTXT.add(magnitudMin);
        configurarVentana();
    }

    private void configurarVentana() {
        ajustarComponentes(getContentPane());
        setResizable(true);
        setSize(400, 250);
        setMinimumSize(new Dimension(400, 250));
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                cerrarVentana();
            }
            
        });
    }

    public void ajustarComponentes(Container c) {
        c.setLayout(new BorderLayout());
        ajustarMenu();
        c.setBackground(new Color(215, 239, 255));
        JPanel principal = new JPanel();
        principal.setLayout(new GridBagLayout());

        // <editor-fold defaultstate="collapsed" desc=" Acomodo de los elementos de esta pantalla ">
        GridBagConstraints gbc = new GridBagConstraints();

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 0.5;
        gbc.ipadx = 10;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        principal.add(new Label("Filtrar por Fechas:"), gbc);

        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.insets = new Insets(0, 60, 0, 0);
        principal.add(new Label("\tFecha Inicial (dd-mm-aaaa): "), gbc);

        gbc.gridx = 1;
        gbc.insets = new Insets(0, 0, 0, 10);
        principal.add(fechaIni, gbc);

        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.insets = new Insets(0, 60, 0, 0);
        principal.add(new Label("\tFecha Final (dd-mm-aaaa): "), gbc);

        gbc.gridx = 1;
        gbc.insets = new Insets(0, 0, 0, 10);
        principal.add(fechaFin, gbc);

        gbc.gridx = 0;
        gbc.gridy = 4;
        principal.add(new Label("Filtrar por Magnitudes"), gbc);

        gbc.gridy = 5;
        gbc.insets = new Insets(0, 60, 0, 0);
        principal.add(new Label("\tMagnitud Minima: "), gbc);

        gbc.gridx = 1;
        gbc.insets = new Insets(0, 0, 0, 10);
        principal.add(magnitudMin, gbc);

        gbc.gridy = 6;
        gbc.gridx = 0;
        gbc.insets = new Insets(0, 60, 0, 0);
        principal.add(new Label("\tMagnitud Maxima: "), gbc);

        gbc.gridx = 1;
        gbc.insets = new Insets(0, 0, 0, 10);
        principal.add(magnitudMax, gbc);

        gbc.gridy = 7;
        gbc.gridx = 0;
        gbc.insets = new Insets(0, 60, 0, 0);
        principal.add(rbCualquierMagnitud, gbc);

// </editor-fold>

        c.add(BorderLayout.NORTH, new JPanel());

        c.add(BorderLayout.CENTER, principal);
        c.add(BorderLayout.SOUTH, btnFiltrar);

        rbCualquierMagnitud.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (!rbCualquierMagnitud.isSelected()) {
                    magnitudMax.setEnabled(true);
                    magnitudMin.setEnabled(true);
                } else {
                    magnitudMax.setEnabled(false);
                    magnitudMin.setEnabled(false);
                }
                validate();
                repaint();
            }
        });

        btnFiltrar.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                aplicarFiltro();
            }
        });

        listTXT.forEach((t) -> {
            t.addFocusListener(new FocusAdapter() {
                @Override
                public void focusGained(FocusEvent e) {
                    t.selectAll();
                }
            });
        });

    }

    public void init() {
        gestorPrincipal.registrar(this);
        setVisible(true);
    }

    private void ajustarMenu() {
        menuPrincipal = new JMenuBar();
        menuPrincipal.add(menuArchivo = new JMenu("Archivo"));
        menuArchivo.add(itemSeleccionarArchivo = new JMenuItem("Seleccionar Fichero de Datos"));
        menuArchivo.add(itemSalir = new JMenuItem("Salir"));
        setJMenuBar(menuPrincipal);

        itemSeleccionarArchivo.addActionListener((e) -> {
            seleccionarArchivoDatos();
        });

        itemSalir.addActionListener((e) -> {
            cerrarPrograma();
        });
    }

    @Override
    public void update(Observable o, Object arg) {

    }

    public void seleccionarArchivoDatos() {
        JFileChooser jfc = new JFileChooser();
        jfc.setFileFilter(new FileFilter() {
            @Override
            public String getDescription() {
                return "Archivos de Texto (*.txt)";
            }

            @Override
            public boolean accept(File f) {
                if (f.isDirectory()) {
                    return true;
                } else {
                    return f.getName().toLowerCase().endsWith(".txt");
                }
            }

        });
        if (jfc.showOpenDialog(jfc) == JFileChooser.APPROVE_OPTION) {
            cargarArchivo(jfc.getSelectedFile());
        }
    }

    public void cargarArchivo(File archivo) {
        gestorPrincipal.cargarTerremotos(archivo);
    }

    public void cerrarPrograma() {
        System.out.println("Cerrando la aplicación..");
        gestorPrincipal.suprimir(this);
        gestorPrincipal.cerrarAplicacion();
    }

    public void cerrarVentana() {
        gestorPrincipal.suprimir(this);
    }

    public void aplicarFiltro() {
        
        HashMap parametros = new HashMap();
        
        parametros.put("FechaInicial", 
                fechaIni.getText().isEmpty() ? "1-1-1000": fechaIni.getText());
        
        parametros.put("FechaFinal", 
                fechaFin.getText().isEmpty() ? "31-12-2120": fechaFin.getText());
        
        parametros.put("MagnitudMinima", 
                magnitudMin.getText().isEmpty() ? "0": magnitudMin.getText());
        
        parametros.put("MagnitudMaxima", 
                magnitudMax.getText().isEmpty() ? "11": magnitudMax.getText());
        
        gestorPrincipal.filtrarTerremotos(parametros);
        System.out.println("Datos enviados para filtrar..");
    }

}
