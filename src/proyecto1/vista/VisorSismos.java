package proyecto1.vista;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.Observer;
import java.awt.Font;
import java.awt.Stroke;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.ToolTipManager;
import proyecto1.modelo.entidades.Terremoto;
import xmlproyecto1.BaseMapa;

public class VisorSismos extends JPanel {

    public static double CONVERTIR_KM = 0.5747126436781;
    public static double CONVERTIR_MI = 0.3597122302158;
    private Icon Mapa;
    private JLabel fondo;
    private Point ubicacionCursor;
    private Point iniArrastre;
    private boolean arrastrando;
    private boolean ocultarTerremotos;
    private final ObservadorCoordenadas notificador;
    private final List<AreaTerremoto> listaTerremotos;
    private final BaseMapa datosXML;

    public VisorSismos(BaseMapa datosXML) {
        listaTerremotos = new ArrayList<>();
        notificador = new ObservadorCoordenadas();
        ubicacionCursor = new Point();
        iniArrastre = new Point();
        arrastrando = false;
        ocultarTerremotos = false;
        this.datosXML = datosXML;
        configurarVisor();
    }

    public void configurarVisor() {
        setSize(853, 667);
        setMinimumSize(new Dimension(853, 667));
        setBackground(new Color(215, 239, 255));
        addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseMoved(MouseEvent e) {
                repintarLineas(e);
                comprobarEnArea(e);
            }

            @Override
            public void mouseDragged(MouseEvent e) {
                repintarLineas(e);
                if (!arrastrando) {
                    iniArrastre = e.getPoint();
                    arrastrando = true;
                }
            }
        });
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                arrastrando = false;
                repintarLineas(e);
            }
        });

        cargarFondo();

        ocultarCursor();
    }

    public void anadirTerremoto(Terremoto t) {
        listaTerremotos.add(new AreaTerremoto(t));
        repaint();
    }

    public void limpiarLista() {
        listaTerremotos.clear();
    }

    public void cargarFondo() {
        try {
            Mapa = new ImageIcon(VisorSismos.class.getResource(datosXML.getImgCR().toString()));
            System.out.println("Cargada Correrctamente");
            fondo = new JLabel(Mapa);
            fondo.setMinimumSize(new Dimension(853, 667));
            add(fondo);
        } catch (Exception ex) {
            System.out.println("Fallo en la carga de la imagen");
        }
    }

    private void ocultarCursor() {
        BufferedImage cursorIMG = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
        Cursor cursorTransp = Toolkit.getDefaultToolkit().createCustomCursor(cursorIMG, new Point(0, 0), "Transparente");
        setCursor(cursorTransp);
    }

    public void repintarLineas(MouseEvent e) {
        ubicacionCursor = e.getPoint();
        repaint();
        notificador.notificar(ubicacionCursor);
    }

    public void registrarObservador(Observer observer) {
        notificador.addObserver(observer);
        System.out.println("Observador registrado en el Visor de Mapa");
    }

    public void comprobarEnArea(MouseEvent e) {
        listaTerremotos.stream().filter((a) -> (a.getAreaTerremoto() != null && a.getAreaTerremoto().contains(ubicacionCursor))).forEachOrdered((aux) -> {
            this.setToolTipText(aux.getTerremoto().toString());
            ToolTipManager.sharedInstance().mouseMoved(e);
        });
    }

    public double calcularDistancia() {
        double x1 = iniArrastre.x, y1 = iniArrastre.y, x2 = ubicacionCursor.x, y2 = ubicacionCursor.y;
        return Math.sqrt((y2 - y1) * (y2 - y1) + (x2 - x1) * (x2 - x1));
    }

    public void ocultar_mostrar_terremotos() {
        ocultarTerremotos = !ocultarTerremotos;
        repaint();
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        int auxX, auxY;
        Graphics2D g2 = (Graphics2D) g;

        // Dibujamos las lineas de los meridianos        
        g2.setColor(Color.BLACK);
        g2.setStroke(new BasicStroke(2));
        //  Seteamos el tipo de fuente para escribir en pantalla
        g2.setFont(new java.awt.Font("SansSerif", Font.BOLD, 14));

        //  Recorremos la lista de coordenadas del BaseMapa
        //  Obtenemos las coordenadas y las utilizamos para dibujar las lineas de los meridianos
        for (int i = 0; i < datosXML.getListCoord().getCoordenadas().size(); i++) {
            auxX = datosXML.getListCoord().getCoordenadas().get(i).getP().getX();
            auxY = datosXML.getListCoord().getCoordenadas().get(i).getP().getY();
            //  Dibujamos primero la linea vertical
            g2.drawLine(fondo.getX() + auxX, 0, fondo.getX() + auxX, getHeight());
            //  Ahora dibujamos la linea horizontal
            g2.drawLine(0, fondo.getY() + auxY, getWidth(), fondo.getY() + auxY);
            //  Escribimos en pantalla la latitud y longuitud de dichos puntos
            g2.drawString(datosXML.getListCoord().getCoordenadas().get(i).getCoordMap().getLatitud().toString(),
                    fondo.getX() + auxX + 10, fondo.getY() + auxY - 25);
            g2.drawString(datosXML.getListCoord().getCoordenadas().get(i).getCoordMap().getLongitud().toString(),
                    fondo.getX() + auxX + 10, fondo.getY() + auxY - 13);
        }

        //  Con este bloque de codigo se grafican los terremotos y sus magnitudes
        //  Comprobamos que la lista de areas no este vacia
        if (!listaTerremotos.isEmpty() && !ocultarTerremotos) {
            //  Ahora recorremos dicha lista y dibujamos cada area de terremoto contenida en ella
            listaTerremotos.forEach((aux) -> {
                //  Seteamos el color del area dependiendo de la escala de richter
                g2.setColor(aux.getColor());
                //  Dibujamos el area
                g2.fill(aux.crearArea(fondo.getX(), fondo.getY()));
                //  Con esto escribimos la magnitud en el centro del area
                g2.setColor(Color.BLACK);
                g2.drawString(String.valueOf(aux.getTerremoto().getMagnitud()),
                        fondo.getX() + aux.getEpicentro().x - 10, fondo.getY() + aux.getEpicentro().y + 5);
            });
        }

        //  Este codigo dibuja la linea roja para convertir y/o demarcar una distacia si se esta haciendo un mouseDrag
        if (arrastrando) {
            g2.setColor(Color.RED.darker().darker());
            g2.setStroke(new BasicStroke(3));
            //  Dibujamos una linea desde el punto de inicio del arrastre hasta la ubicaicon del cursor
            g2.drawLine(iniArrastre.x, iniArrastre.y, ubicacionCursor.x, ubicacionCursor.y);
            //  Escribimos arriba a la derecha del cursor las millas y kilometros
            //  convertidos en la escala del mapa
            g2.drawString((int) (calcularDistancia() * CONVERTIR_MI) + " Mi", ubicacionCursor.x + 10, ubicacionCursor.y - 10);
            g2.drawString((int) (calcularDistancia() * CONVERTIR_KM) + " KM", ubicacionCursor.x + 10, ubicacionCursor.y - 25);

        }

        g2.setColor(Color.GREEN.darker());
        //  Seteamos un stroke para las lineas guia
        Stroke dashed = new BasicStroke(3, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[]{9}, 0);
        g2.setStroke(dashed);
        //  Guia Vertical
        g2.drawLine(ubicacionCursor.x, 0, ubicacionCursor.x, getHeight());
        //  Guia Horizontal
        g2.drawLine(0, ubicacionCursor.y, getWidth(), ubicacionCursor.y);

        //  Seteamos un stroke solido para el cursor
        g2.setColor(Color.RED);
        g2.setStroke(new BasicStroke(3));
        //  Cursor Vertical
        g2.drawLine(ubicacionCursor.x, ubicacionCursor.y - 10, ubicacionCursor.x, ubicacionCursor.y + 10);
        //  Cursor Horizontal
        g2.drawLine(ubicacionCursor.x - 10, ubicacionCursor.y, ubicacionCursor.x + 10, ubicacionCursor.y);

    }

}
