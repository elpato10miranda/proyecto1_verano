package proyecto1;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.xml.bind.JAXBException;
import proyecto1.control.ControlProyecto;
import xmlproyecto1.BaseMapa;
import proyecto1.vista.VentanaAplicacion;

/**
 *
 * @author Matthew Miranda
 */
public class PROYECTO1 {

    public static void main(String[] args) throws JAXBException {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            JFrame.setDefaultLookAndFeelDecorated(true);
            datos = xmlproyecto1.XMLProyecto1.leerXML();
            cargado = true;
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | UnsupportedLookAndFeelException
                | JAXBException e) {
            System.err.printf("Excepción: '%s'%n", e.getMessage());
            cargado = false;
        }

        mostrarInterfaz();

    }

    public static void mostrarInterfaz() {
        SwingUtilities.invokeLater(() -> {
            System.out.println("Abriendo interfaz grafica.");
            new VentanaAplicacion("MAPA SISMOLÓGICO", new ControlProyecto(datos), cargado).init();
        });
    }

    private static boolean cargado;
    private static BaseMapa datos;
}
