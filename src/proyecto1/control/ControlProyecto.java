package proyecto1.control;

import java.io.File;
import java.util.HashMap;
import java.util.Observer;
import proyecto1.modelo.ModeloMapa;
import xmlproyecto1.BaseMapa;

/**
 *
 * @author Matthew Miranda
 */
public class ControlProyecto {

    private ModeloMapa datos;    

    public ControlProyecto(ModeloMapa datos) {
        this.datos = datos;
    }

    public ControlProyecto(BaseMapa datosXML) {
        this(new ModeloMapa(datosXML));
    }
    
    public void registrar(Observer nuevoObservador) {
        System.out.printf("Registrando: %s..%n", nuevoObservador);
        datos.addObserver(nuevoObservador);
    }

    public void suprimir(Observer actual) {
        System.out.printf("Suprimiendo: %s..%n", actual);
        datos.deleteObserver(actual);
    }

    public void cerrarAplicacion() {
        System.out.println("Aplicación finalizada exitosamente..");
        System.exit(0);
    }

    public void cargarTerremotos(File archivo) {
        datos.cargarDatosTerremotos(archivo);
    }
    
    public Object obtenerListaTerremotos(){
        return datos.obtenerListaTerremotos();
    }
    
    public void filtrarTerremotos(HashMap parametros){
        datos.filtrarTerremotos(parametros);
    }

    public void reiniciarDatos() {
        datos.reiniciarDatos();
    }
    
    public BaseMapa obtenerDatosXML(){
        return datos.getDatosXML();
    }
    
}
