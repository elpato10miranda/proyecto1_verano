
package proyecto1.modelo.entidades;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Observable;

/**
 *
 * @author Kenneth Sibaja
 */
public class ListaTerremotos extends Observable {

    private List<Terremoto> listaTerremotos;

    public ListaTerremotos() {
        listaTerremotos = new ArrayList<>();
    }

    public List<Terremoto> getListaTerremotos() {
        return listaTerremotos;
    }

    public void setListaTerremotos(List<Terremoto> listaTerremotos) {
        this.listaTerremotos = listaTerremotos;
    }

    public int cantidadTerremotos() {
        return listaTerremotos.size();
    }

    public Terremoto obtenerTerremoto(int i) {
        return listaTerremotos.get(i);
    }

    public void borrar() {
        listaTerremotos.clear();
        setChanged();
        notifyObservers();
    }

    public void agregar(Terremoto t) {
        listaTerremotos.add(t);
        setChanged();
        notifyObservers(t);
    }
    
    public boolean esVacia(){
        return listaTerremotos.isEmpty();
    }
    
    public void imprimirLista(){
        if (!listaTerremotos.isEmpty()) {
            listaTerremotos.forEach((t) -> {
                System.out.println(t.toString());
            });
        } else {
            System.out.println("Lista de Terremotos vacia.");
        }
    }
    
    public ListaTerremotos obtenerModelo() {
        return this;
    }
    
    public void filtrarTerremotos(HashMap parametros) {
        double magniMax, magniMin;
        List<Terremoto> listAux = new ArrayList<>();
        LocalDate fechaIni, fechaFin;
        Date fechaAux;
        try {
            fechaAux = new SimpleDateFormat("dd-MM-yyyy").parse((String) parametros.get("FechaInicial"));
            fechaIni = fechaAux.toInstant()
                    .atZone(ZoneId.systemDefault())
                    .toLocalDate();
            fechaAux = new SimpleDateFormat("dd-MM-yyyy").parse((String) parametros.get("FechaFinal"));
            fechaFin = fechaAux.toInstant()
                    .atZone(ZoneId.systemDefault())
                    .toLocalDate();
            magniMin = Double.parseDouble((String) parametros.get("MagnitudMinima"));
            magniMax = Double.parseDouble((String) parametros.get("MagnitudMaxima"));
            
            listaTerremotos.stream().filter((t) -> (t.getFecha().isBefore(fechaFin)
                    && t.getFecha().isAfter(fechaIni)
                    && t.getMagnitud() >= magniMin
                    && t.getMagnitud() <= magniMax)).forEachOrdered((t) -> {
                listAux.add(t);
            });
            listaTerremotos = listAux;
        } catch (NumberFormatException | ParseException ex) {
            System.err.println("Something´s wrong, i can feel it");
        }
    }
    
}
