/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto1.modelo.entidades;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import proyecto1.utils.ConversorGrados;
import xmlproyecto1.Latitud;
import xmlproyecto1.Longitud;

/**
 *
 * @author Kenneth Sibaja
 */
public class Terremoto {

    private int registro;
    private int registroAnual;
    private LocalDate fecha;
    private Longitud longitud;
    private Latitud latitud;
    private double magnitud;
    private double profundidad;

    public Terremoto(int registro, int registroAnual, String fecha, double longitud, double latitud, double magnitud, double profundidad) throws ParseException {

        this.registro = registro;
        this.registroAnual = registroAnual;
        this.longitud = ConversorGrados.convertirLongitud_Sexagecimal(longitud);
        this.latitud = ConversorGrados.convertirLatitud_Sexagecimal(latitud);
        this.magnitud = magnitud;
        this.profundidad = profundidad;
        Date fechaAux = new SimpleDateFormat("dd-MM-yyyy").parse(fecha);
        this.fecha = fechaAux.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
    }

    public Object[] toArray() {
        Object[] r = new Object[7];
        r[0] = getRegistro();
        r[1] = getRegistroAnual();
        r[2] = getFecha();
        r[3] = obtenerLatitud();
        r[4] = obtenerLongitud();
        r[5] = getMagnitud();
        r[6] = getProfundidad();
        return r;
    }

    public int getRegistro() {
        return registro;
    }

    public void setRegistro(int registro) {
        this.registro = registro;
    }

    public int getRegistroAnual() {
        return registroAnual;
    }

    public void setRegistroAnual(int registroAnual) {
        this.registroAnual = registroAnual;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public String obtenerLongitud() {
        return longitud.toString();
    }

    public void setLongitud(double longitud) {
        this.longitud = ConversorGrados.convertirLongitud_Sexagecimal(longitud);
    }

    public String obtenerLatitud() {
        return latitud.toString();
    }

    public void setLatitud(double latitud) {
        this.latitud = ConversorGrados.convertirLatitud_Sexagecimal(latitud);
    }

    public double getMagnitud() {
        return magnitud;
    }

    public void setMagnitud(double magnitud) {
        this.magnitud = magnitud;
    }

    public double getProfundidad() {
        return profundidad;
    }

    public void setProfundidad(double profundidad) {
        this.profundidad = profundidad;
    }

    public static int getFieldCount() {
        return 7;
    }

    public Longitud getLongitud() {
        return longitud;
    }

    public Latitud getLatitud() {
        return latitud;
    }

    @Override
    public String toString() {
        return "Terremoto {  fecha: " + fecha
                + ", Latitud: " + latitud.toString() + ", Longitud: " + longitud.toString() 
                + "\n Magnitud: " + magnitud + ", Profundidad: " + profundidad + "  };";
    }

}
