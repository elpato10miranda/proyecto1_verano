package proyecto1.modelo;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Observable;
import java.util.Scanner;
import proyecto1.modelo.entidades.ListaTerremotos;
import proyecto1.modelo.entidades.Terremoto;
import xmlproyecto1.BaseMapa;

/**
 *
 * @author Matthew Miranda
 */
public class ModeloMapa extends Observable {

    private ListaTerremotos listaTerremotosRespaldo;
    private ListaTerremotos listaTerremotos;
    private BaseMapa datosXML;

    public ModeloMapa(BaseMapa datosXML) {
        System.out.println("Inicializando modelo..");
        this.datosXML = datosXML;
        listaTerremotos = new ListaTerremotos();
        listaTerremotosRespaldo = new ListaTerremotos();
    }

    private void actualizarDatos() {
        setChanged();
        notifyObservers(listaTerremotos);
    }

    public void cargarDatosTerremotos(File archivo) {
        int ind, indanu, cont = 0;
        String f;
        double lati, longui, mag, prof;
        List<Terremoto> listAux = new ArrayList<>();
        try (Scanner lector = new Scanner(archivo)) {
            lector.useDelimiter("[\t\n]");
            while (lector.hasNext()) {
                listAux.add(new Terremoto(
                        lector.nextInt(),
                        lector.nextInt(),
                        lector.next(),
                        lector.nextDouble(),
                        lector.nextDouble(),
                        lector.nextDouble(),
                        lector.nextDouble()));
            }
            listaTerremotos.setListaTerremotos(listAux);
            for (int i = 0; i < listaTerremotos.cantidadTerremotos(); i++) {
                listaTerremotosRespaldo.agregar(listaTerremotos.obtenerTerremoto(i));
            }
            actualizarDatos();
        } catch (Exception ex) {
            System.out.println("Error al leer el archivo.");
            System.out.println("Error: " + ex.getMessage() + "\n\n");
        }
    }

    public BaseMapa getDatosXML() {
        return datosXML;
    }

    public ListaTerremotos obtenerListaTerremotos() {
        return listaTerremotos;
    }

    public void filtrarTerremotos(HashMap parametros) {
        restaurarLista();
        listaTerremotos.filtrarTerremotos(parametros);
        actualizarDatos();
    }

    public void restaurarLista() {
        listaTerremotos.getListaTerremotos().clear();
        for (int i = 0; i < listaTerremotosRespaldo.cantidadTerremotos(); i++) {
            listaTerremotos.agregar(listaTerremotosRespaldo.obtenerTerremoto(i));
        }
    }

    public void reiniciarDatos() {
        listaTerremotos = listaTerremotosRespaldo;
        actualizarDatos();
    }

}
