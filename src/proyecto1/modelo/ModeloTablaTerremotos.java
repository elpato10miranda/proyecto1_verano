/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto1.modelo;

import java.text.DecimalFormat;
import java.util.Observable;
import java.util.Observer;
import javax.swing.table.AbstractTableModel;
import proyecto1.modelo.entidades.ListaTerremotos;
import proyecto1.modelo.entidades.Terremoto;

/**
 *
 * @author Kenneth Sibaja
 */
public class ModeloTablaTerremotos extends AbstractTableModel implements Observer{
    
    private ListaTerremotos terremotos;
    private DecimalFormat fmt = new DecimalFormat("#,##0.00");
    
    public ModeloTablaTerremotos(ListaTerremotos lista){
        terremotos = lista;
        System.out.println("Modelo de tabla creado");
    }
    
    @Override
    public int getRowCount() {
        return terremotos.cantidadTerremotos();
    }

    @Override
    public int getColumnCount() {
        return Terremoto.getFieldCount();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object r = terremotos.obtenerTerremoto(rowIndex).toArray()[columnIndex];
        if (r instanceof Number) {
            r = fmt.format(r);
        }
        return r;
    }
    
    public void actualizar() {
        fireTableDataChanged();
    }

    @Override
    public void update(Observable o, Object arg) {
        if(o.getClass().equals(ModeloMapa.class)){
            terremotos = (ListaTerremotos) arg;
            actualizar();
        }
    }
    
}
